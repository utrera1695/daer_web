import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';

import Contact from '../contact/contact';
import './cart.css';
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }
  componentDidMount() {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart) {
      this.setState({ products: cart });
    }
  }
  getTotal() {
    var total = 0;
    this.state.products.forEach(data => {
      total += data.count * data.sale_price;
    });
    console.log(total);
    return total;
  }
  render() {
    return (
      <>
        <div className='cart'>
          <h1>
            <b>Carrito de</b> compras
          </h1>
          <div className='container'>
            <table class='table'>
              <thead>
                <tr>
                  <th scope='col'>#</th>
                  <th scope='col'>Producto</th>
                  <th scope='col'>Precio</th>
                  <th scope='col'>Cantidad</th>
                  <th scope='col'>Total</th>
                </tr>
              </thead>
              <tbody>
                {this.state.products.map((data, index) => (
                  <tr>
                    <th scope='row'>{index + 1}</th>
                    <td>{data.name}</td>
                    <td>{data.sale_price}</td>
                    <td>{data.count}</td>
                    <td>{data.count * data.sale_price} $</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div>
              <h3 style={{ textAlign: 'end' }}>Total: {this.getTotal()}$</h3>
            </div>
          </div>
        </div>

        <Contact />
      </>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default compose(connect(mapStateToProps, mapDispatchToProps))(Cart);

import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-translate';
import './products.css';
import { Form, Card, Modal, Button } from 'react-bootstrap';

import api from '../../providers/api';
import Provider from '../../providers/services';
import Glide from '@glidejs/glide';
import { Redirect, useHistory, Link } from 'react-router-dom';
class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      category: [],
      sub_category: [],
      category_select: '',
      subcategory_select: '',
      search: '',
      show: false,
      product: null
    };
    this.getProducts();
  }
  getProducts() {
    Provider.getProducts().then(res => {
      console.log(res);
      this.setState({
        products: res.data.products,
        category: res.data.category
      });
      new Glide('.glide_products', {
        type: 'slider',
        perView: 4
      }).mount();
    });
  }
  changeCategory(e) {
    this.setState(
      { category_select: e.target.value },
      this.getValidSubCategory()
    );
    new Glide('.glide_products', {
      type: 'slider',
      perView: 4
    }).mount();
  }
  changeSubCategory(e) {
    this.setState({ subcategory_select: e.target.value });
    new Glide('.glide_products', {
      type: 'slider',
      perView: 4
    }).mount();
  }
  changeSearch(e) {
    this.setState({ search: e.target.value });
    new Glide('.glide_products', {
      type: 'slider',
      perView: 4
    }).mount();
  }
  getValidSubCategory() {
    let subs = this.state.products
      .filter(a => a.nameCategory !== this.state.category_select)
      .map(a => {
        return a.nameSubCategory;
      });
    this.setState({
      sub_category: subs.length > 1 ? subs.filter((a, b) => a === b) : subs
    });
  }
  handleClose() {
    this.setState({ product: null, show: false });
  }
  handleOpen(id) {
    Provider.getProduct(id).then(res => {
      console.log(res.data.product[0]);
      this.setState({ product: res.data.product[0], show: true });
      new Glide('.glide_product', {
        type: 'slider',
        perView: 1
      }).mount();
    });
  }
  addToCart(product) {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart) {
      if (cart.filter(a => a.id === product.id) <= 0) {
        cart.push(product);
        localStorage.setItem('cart', JSON.stringify(cart));
      } else {
        cart.map(a => a.count++);
        localStorage.setItem('cart', JSON.stringify(cart));
      }
    } else {
      cart = [product];
      localStorage.setItem('cart', JSON.stringify(cart));
    }
    this.props.setBadge(cart.length);
    this.handleClose();
    window.location.assign('/cart');
  }
  render() {
    let { t } = this.props;
    return (
      <>
        <div className='products'>
          <h1>
            <b>{t('our')}</b> {t('products')}
          </h1>
          <div className='container'>
            <div className='row'>
              <div className='col-3'>
                <Form.Group controlId='exampleForm.ControlSelect2'>
                  <Form.Control
                    size='lg'
                    as='select'
                    value={this.state.category_select}
                    onChange={this.changeCategory.bind(this)}
                  >
                    <option value='' disabled>
                      Categorias
                    </option>
                    {this.state.category.map(data => (
                      <option key={data.id} value={data.name}>
                        {data.name}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </div>
              <div className='col-3'>
                <Form.Group controlId='exampleForm.ControlSelect2'>
                  <Form.Control
                    size='lg'
                    as='select'
                    value={this.state.subcategory_select}
                    onChange={this.changeSubCategory.bind(this)}
                  >
                    <option value='' disabled>
                      Sub Categorias
                    </option>
                    {this.state.sub_category.map((data, index) => (
                      <option key={index} value={data}>
                        {data}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </div>
              <div className='col-6'>
                <Form.Group controlId='formBasicEmail'>
                  <Form.Control
                    value={this.state.search}
                    onChange={this.changeSearch.bind(this)}
                    size='lg'
                    type='text'
                    placeholder={'Buscar'}
                  />
                </Form.Group>
              </div>
            </div>
            <h3 className='search_number'>
              {this.state.products
                .filter(a =>
                  this.state.category_select === ''
                    ? a
                    : a.nameCategory === this.state.category_select
                )
                .filter(a =>
                  this.state.subcategory_select === ''
                    ? a
                    : a.nameSubCategory === this.state.subcategory_select
                )
                .filter(
                  a =>
                    a.name
                      .toLowerCase()
                      .indexOf(this.state.search.toLowerCase()) >= 0
                ).length > 0
                ? this.state.products.length +
                  (this.state.products.length > 1
                    ? ' productos encontrados'
                    : ' producto encontrado')
                : 'No hay productos disponibles'}{' '}
            </h3>

            <div>
              <div className='glide glide_products'>
                <div className='glide__track' data-glide-el='track'>
                  <ul className='glide__slides'>
                    {this.state.products
                      .filter(a =>
                        this.state.category_select === ''
                          ? a
                          : a.nameCategory === this.state.category_select
                      )
                      .filter(a =>
                        this.state.subcategory_select === ''
                          ? a
                          : a.nameSubCategory === this.state.subcategory_select
                      )
                      .filter(
                        a =>
                          a.name
                            .toLowerCase()
                            .indexOf(this.state.search.toLowerCase()) >= 0
                      )
                      .map(data => (
                        <li className='glide__slide' key={data.id}>
                          <Card
                            onClick={() => this.handleOpen(data.id)}
                            style={{ cursor: 'pointer' }}
                          >
                            <Card.Img
                              variant='top'
                              src={
                                api.images + 'products/' + data.img_principal
                              }
                            />
                            <Card.Body>
                              <Card.Title>{data.name}</Card.Title>
                              <Card.Text>{data.description}</Card.Text>
                            </Card.Body>
                          </Card>
                        </li>
                      ))}
                  </ul>
                </div>

                <div className='glide__arrows' data-glide-el='controls'>
                  <button
                    className='glide__arrow glide__arrow--left'
                    data-glide-dir='<'
                  >
                    <i className='fas fa-chevron-left'></i>
                  </button>
                  <button
                    className='glide__arrow glide__arrow--right'
                    data-glide-dir='>'
                  >
                    <i className='fas fa-chevron-right'></i>
                  </button>
                </div>
              </div>
            </div>
            <div></div>
          </div>
        </div>
        {this.state.show ? (
          <Modal
            size='lg'
            show={this.state.show}
            onHide={this.handleClose.bind(this)}
          >
            <Modal.Body>
              <div>
                <div className='glide glide_product'>
                  <div className='glide__track' data-glide-el='track'>
                    <ul className='glide__slides'>
                      <li
                        style={{ display: 'flex', justifyContent: 'center' }}
                        className='glide__slide'
                      >
                        <img
                          style={{ width: '30%' }}
                          src={
                            api.images +
                            'products/' +
                            this.state.product.img_principal
                          }
                          alt=''
                        />
                      </li>
                      <li
                        style={{ display: 'flex', justifyContent: 'center' }}
                        className='glide__slide'
                      >
                        <img
                          style={{ width: '30%' }}
                          src={
                            api.images +
                            'products/' +
                            this.state.product.img_detail1
                          }
                          alt='principal'
                        />
                      </li>
                      <li
                        style={{ display: 'flex', justifyContent: 'center' }}
                        className='glide__slide'
                      >
                        <img
                          style={{ width: '30%' }}
                          src={
                            api.images +
                            'products/' +
                            this.state.product.img_detail2
                          }
                          alt='detalle 2'
                        />
                      </li>
                    </ul>
                  </div>

                  <div className='glide__arrows' data-glide-el='controls'>
                    <button
                      className='glide__arrow glide__arrow--left'
                      data-glide-dir='<'
                    >
                      <i className='fas fa-chevron-left'></i>
                    </button>
                    <button
                      className='glide__arrow glide__arrow--right'
                      data-glide-dir='>'
                    >
                      <i className='fas fa-chevron-right'></i>
                    </button>
                  </div>
                </div>
              </div>
              <div className='container'>
                <h3 style={{ color: '#00afef', fontWeight: 'bold' }}>
                  {this.state.product.name}
                </h3>
                <h4 style={{ fontWeight: 'lighter' }}>
                  {this.state.product.sale_price} $
                </h4>
                <p>{this.state.product.description}</p>
                <div className='row' style={{ justifyContent: 'space-evenly' }}>
                  <Button
                    href={
                      api.images +
                      'products/' +
                      this.state.product.data_sheet_pdf
                    }
                    download
                    variant='secundary'
                    style={{ width: 'auto' }}
                  >
                    Ver ficha tecnica
                  </Button>
                  <Button
                    variant='primary'
                    onClick={() => this.addToCart(this.state.product)}
                  >
                    Agregar al carrito
                  </Button>
                </div>
              </div>
            </Modal.Body>
          </Modal>
        ) : null}
      </>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
  setBadge(n) {
    dispatch({
      type: 'SET_BADGE',
      badge: n
    });
  }
});
export default compose(
  translate('title'),
  connect(mapStateToProps, mapDispatchToProps)
)(Products);

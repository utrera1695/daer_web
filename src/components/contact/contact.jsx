import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-translate';
import './contact.css';
import { Form, Button } from 'react-bootstrap';
class Contect extends Component {
  render() {
    let { t } = this.props;
    return (
      <>
        <div className='contact_form'>
          <h1>{t('contact')}</h1>
          <div className='container '>
            <Form>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control
                  size='lg'
                  type='text'
                  placeholder={'Nombre y Apellido'}
                />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control
                  size='lg'
                  type='text'
                  placeholder={'Razón Social'}
                />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control size='lg' type='text' placeholder={'Cuit'} />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control size='lg' type='text' placeholder={'Teléfono'} />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control
                  size='lg'
                  type='text'
                  placeholder={'Dirección de Correo'}
                />
              </Form.Group>
              <Form.Group controlId='formBasicEmail'>
                <Form.Control
                  size='lg'
                  as='textarea'
                  rows='4'
                  placeholder={'Mensaje'}
                />
              </Form.Group>
              <Button variant='secundary' type='submit'>
                Enviar
              </Button>
            </Form>
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default compose(
  translate('buttons'),
  translate('title'),
  connect(mapStateToProps, mapDispatchToProps)
)(Contect);

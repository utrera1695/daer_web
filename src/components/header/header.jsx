import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Navbar, NavLink } from 'react-bootstrap';
import logo from '../../assets/images/logo_azul.png';
import './header.css';
import { Link } from 'react-router-dom';
class Header extends Component {
  render() {
    return (
      <>
        <Navbar>
          <Navbar.Brand href='/'>
            <img src={logo} style={{ width: '120px' }} alt='logo daer' />
          </Navbar.Brand>
          <div className='nav-icons'>
            <i className='fas fa-search nav-icon'></i>
            <hr className='separator'></hr>
            <Link to='/cart'>
              <i className='fas fa-shopping-cart nav-icon'>
                {this.props.badge > 0 ? (
                  <span class='badge'>{this.props.badge}</span>
                ) : null}
              </i>
            </Link>
          </div>
        </Navbar>
      </>
    );
  }
}
const mapStateToProps = state => ({
  badge: state.badge
});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(Header);

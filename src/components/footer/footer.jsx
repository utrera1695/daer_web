import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-translate';
import logo from '../../assets/images/logo_azul.png';
import './footer.css';
class Footer extends Component {
  render() {
    let { t } = this.props;
    return (
      <>
        <div className='footer-content'>
          <div className='footer__brand'>
            <img src={logo} style={{ width: '120px' }} alt='logo daer' />
            <div className='nav-icons'>
              <i className='fab fa-facebook-f nav-icon'></i>
              <i className='fab fa-instagram nav-icon'></i>
            </div>
          </div>
          <div className='container'>
            <div className='row'>
              <div className='col-sm-4'>
                <div className='info'>
                  <i className='fas fa-phone-alt icon'></i>
                  <div>
                    <p>(011) 4451- 7536</p>
                  </div>
                </div>
                <div className='info'>
                  <i className='fas fa-at icon'></i>
                  <div>
                    <p>CDANIEL2R@GMAIL.COM</p>
                    <p>ANNYRR@HOTMAIL.COM</p>
                  </div>
                </div>
                <div className='info'>
                  <i className='fas fa-map-marked-alt icon'></i>
                  <div>
                    <p>CESAR MALNATTI 1750 </p>
                    <p>SAN MIGUEL</p>
                    <p>BUENOS AIRES ARGENTINA</p>
                    <p>CARLOS RODRIGUEZ</p>
                  </div>
                </div>
              </div>
              <div className='col-sm-4 footer_menu'>
                <h4>{t('home')}</h4>
                <h4>
                  {t('about')} {t('us')}
                </h4>
                <h4>{t('products')}</h4>
              </div>
              <div className='col-sm-4 footer_menu'>
                <h4>{t('quality')}</h4>
                <h4>{t('news')}</h4>
                <h4>{t('online_inquiries')}</h4>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default compose(
  translate('title'),
  connect(mapStateToProps, mapDispatchToProps)
)(Footer);

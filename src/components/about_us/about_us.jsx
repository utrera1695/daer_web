import React, { Component } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { translate } from 'react-translate';

import Glide from '@glidejs/glide';
import Split from '../../assets/images/aire_split.png';
import './about_us.css';
import { Card } from 'react-bootstrap';

class AboutUs extends Component {
  componentDidMount() {
    new Glide('.glide_about_us', {
      type: 'carousel',
      focusAt: 'center',
      perView: 3,
      hoverpause: true
    }).mount();
  }
  render() {
    let { t } = this.props;
    return (
      <>
        <div className='about_us'>
          <h1>
            <b>Acerca de</b> nosotros
          </h1>
          <div className='container'>
            <div className='row'>
              <div className='col-12 col-sm-4' style={{ marginBottom: '20px' }}>
                <Card>
                  <Card.Body>
                    <i className='fas fa-crosshairs icon'></i>
                    <Card.Title>{t('mision')}</Card.Title>
                    <Card.Text>{t('mision_text')}</Card.Text>
                  </Card.Body>
                </Card>
              </div>
              <div className='col-12 col-sm-4' style={{ marginBottom: '20px' }}>
                <Card>
                  <Card.Body>
                    <i className='fas fa-eye icon'></i>
                    <Card.Title>{t('vision')}</Card.Title>
                    <Card.Text>{t('vision_text')}</Card.Text>
                  </Card.Body>
                </Card>
              </div>
              <div className='col-12 col-sm-4' style={{ marginBottom: '20px' }}>
                <Card>
                  <Card.Body>
                    <i className='fas fa-gem icon'></i>
                    <Card.Title>{t('values')}</Card.Title>
                    <Card.Text>{t('values_text')}</Card.Text>
                  </Card.Body>
                </Card>
              </div>
            </div>
            <div className='glide glide_about_us'>
              <div className='glide__track' data-glide-el='track'>
                <ul className='glide__slides'>
                  <li className='glide__slide'>
                    <img
                      src='http://redalimentaria.net/wp-content/uploads/2016/11/INDUSTRIAS-DAER_IMG_8975-768x512.jpg'
                      alt='empresa'
                    />{' '}
                  </li>
                  <li className='glide__slide'>
                    <img
                      src='http://redalimentaria.net/wp-content/uploads/2016/11/INDUSTRIAS-DAER_IMG_8975-768x512.jpg'
                      alt='empresa'
                    />
                  </li>
                  <li className='glide__slide'>
                    <img
                      src='http://redalimentaria.net/wp-content/uploads/2016/11/INDUSTRIAS-DAER_IMG_8975-768x512.jpg'
                      alt='empresa'
                    />
                  </li>
                </ul>
              </div>

              <div className='glide__arrows' data-glide-el='controls'>
                <button
                  className='glide__arrow glide__arrow--left'
                  data-glide-dir='<'
                >
                  <i className='fas fa-chevron-left'></i>
                </button>
                <button
                  className='glide__arrow glide__arrow--right'
                  data-glide-dir='>'
                >
                  <i className='fas fa-chevron-right'></i>
                </button>
              </div>
              <div className='glide__bullets' data-glide-el='controls[nav]'>
                <button className='glide__bullet' data-glide-dir='=0'></button>
                <button className='glide__bullet' data-glide-dir='=1'></button>
                <button className='glide__bullet' data-glide-dir='=2'></button>
              </div>
            </div>
          </div>

          <img className='split' src={Split} alt='aire split' />
        </div>
      </>
    );
  }
}
const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default compose(
  translate('title'),
  translate('about_us'),
  connect(mapStateToProps, mapDispatchToProps)
)(AboutUs);

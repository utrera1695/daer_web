import {
  createStore
} from 'redux'

const initialState = {
  lang: 'es',
  home: [],
  galleryCustomer: [],
  badge: 0
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'HOME': {
      return ({
        ...state,
        home: action.data.galleryHome,
        galleryCustomer: action.data.galleryCustomer
      })
    }
    case 'SET_BADGE': {
      return ({
        ...state,
        badge: action.badge
      })
    }
    default: {
      return {
        ...state,
      }
    }
  }
}

export default createStore(reducer)
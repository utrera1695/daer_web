import React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import './App.css';
import Components from './components/components';

function App() {
  return (
    <Provider store={store}>
      <div className='App'>
        <Components />
      </div>
    </Provider>
  );
}

export default App;

import api from './api'
import axios from 'axios';


const getHomeContent = async () => {
  return await axios.get(api.url + 'home')
}

const getQuality = async () => {
  return await axios.get(api.url + 'quality')
}

const getProducts = async () => {
  return await axios.get(api.url + 'products?typeFilter=1')
}

const getProduct = async (id) => {
  return await axios.get(api.url + 'products/' + id)
}

export default {
  getHomeContent,
  getQuality,
  getProducts,
  getProduct
}